/* A simple server in the internet domain using TCP
   The port number is passed as an argument 
   This version runs forever, forking off a separate 
   process for each connection
*/

/* Codebook:
*
* Code | Action
* -----+---------
* 1      dockHDD()
* 2      undockHDD()
* 3      powerHDD()
* 4      unpowerHDD()
*/

/* compile with: gcc server_multi.c -o server_multi -I/usr/local/include -L/usr/local/lib -lwiringPi */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <wiringPi.h>

/* BPi pin definitions */
#define RELAIS_MOTOR 0
#define RELAIS_HDD 1
#define MOTOR_TERMINAL1 2
#define MOTOR_TERMINAL2 3
#define SENSOR_DOCK 4
#define SENSOR_UNDOCK 5

#define RELAIS_OPEN 0
#define RELAIS_CLOSE 1

#define BACKWARD 2
#define FORWARD 1
#define BRAKE 0

/* function prototypes */
void dostuff(int);
void init_ports(void);
void relais(int relais, int mode);
int sensor (int sensor_pin);
int dockHDD(float timeout);
int undockHDD(float timeout);
void motor(int mode);
int powerHDD(void);
void unpowerHDD(void);

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
	init_ports();

     int sockfd, newsockfd, portno, pid;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;

     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     while (1) {
         newsockfd = accept(sockfd, 
               (struct sockaddr *) &cli_addr, &clilen);
         if (newsockfd < 0) 
             error("ERROR on accept");
         pid = fork();
         if (pid < 0)
             error("ERROR on fork");
         if (pid == 0)  {
             close(sockfd);
             dostuff(newsockfd);
             exit(0);
         }
         else close(newsockfd);
     } /* end of while */
     close(sockfd);
     return 0; /* we never get here */
}

/******** DOSTUFF() *********************
 There is a separate instance of this function 
 for each connection.  It handles all communication
 once a connnection has been established.
 *****************************************/
void dostuff (int sock)
{
   int n;
   char buffer[256];
   char message_str;
   int message_code = 0;
      
   bzero(buffer,256);
   n = read(sock,buffer,255);
   if (n < 0) error("ERROR reading from socket");
   printf("Here is the message: %s\n",buffer);
   message_str = buffer[1];
   message_code = atoi(&message_str);
   printf("Messagecode: %i\n", message_code);

   /* Here is where the fun begins */
   switch (message_code) {
   	case 1:
   		printf("Received code 1, performing dockHDD()\n");
      dockHDD(100);
   		break;

   	case 2:
      printf("Received code 2, performing undockHDD()\n");
      undockHDD(100);
   		break;

    case 3:
      printf("Received code 3, powering HDD\n");
      powerHDD();
      break;

    case 4:
      printf("Received code 4, unpowering HDD\n");
      unpowerHDD();
      break;

   	case 5:
      /* turn everything on */
      digitalWrite(RELAIS_MOTOR,1);
      digitalWrite(RELAIS_HDD,1);
      digitalWrite(MOTOR_TERMINAL1,1);
      digitalWrite(MOTOR_TERMINAL2,1);
      delay(1000);
      /* turn everything off */
      digitalWrite(RELAIS_MOTOR,0);
      digitalWrite(RELAIS_HDD,0);
      digitalWrite(MOTOR_TERMINAL1,0);
      digitalWrite(MOTOR_TERMINAL2,0);
   		break;

    case 6:
      /* wait for button press */
      while(1) {
        if(sensor(SENSOR_DOCK)) {
          if(digitalRead(RELAIS_MOTOR))
            digitalWrite(RELAIS_MOTOR, 0);
          else
            digitalWrite(RELAIS_MOTOR, 1);
        }
        if(sensor(SENSOR_UNDOCK)) {
          if(digitalRead(RELAIS_HDD))
            digitalWrite(RELAIS_HDD, 0);
          else
            digitalWrite(RELAIS_HDD, 1);
          break;
        }
      }
      break;

   	default:
   		printf("invalid message_code\n");
   }

   n = write(sock,"I got your message",18);
   if (n < 0) error("ERROR writing to socket");
}

void init_ports(void) {
  wiringPiSetup();

  /* Relais open at default */
  pinMode(RELAIS_MOTOR,OUTPUT);
  pinMode(RELAIS_HDD,OUTPUT);
  digitalWrite(RELAIS_MOTOR, RELAIS_OPEN);
  digitalWrite(RELAIS_HDD, RELAIS_OPEN);

  /* Motor Driver */
  pinMode(MOTOR_TERMINAL1,OUTPUT);
  pinMode(MOTOR_TERMINAL2,OUTPUT);
  digitalWrite(MOTOR_TERMINAL1, 0);
  digitalWrite(MOTOR_TERMINAL2, 0);

  //pinMode(14,OUTPUT);

  /* turn the internal pullup-resistors for the sensor-pins on */
  pinMode(SENSOR_DOCK,INPUT);
  pinMode(SENSOR_UNDOCK,INPUT);
  pullUpDnControl(SENSOR_DOCK, PUD_UP);
  pullUpDnControl(SENSOR_UNDOCK, PUD_UP);

  /* next line sets pin1 in pwm mode */
  // pinMode(1,2);

  /* set GPIO PIN 2 (16 on Board) as Input for the ADC sense */
  // pinMode(16,0);
}

/* Closes the relais with mode 1 or opens it with mode 0 */
void relais(int relais, int mode) {
  /* the Relaisboard contains an inverter, so a logical LOW results in an open relais vice versa */
  if(mode == 0) {
    digitalWrite(relais,0);
  }
  else if(mode == 1) {
    digitalWrite(relais,1);
  }
  return;
}

/* Returns 1 if sensor on sensor_pin is pressed, otherwise 0 */
int sensor (int sensor_pin) {
  if(digitalRead(sensor_pin)==1) {
    return 0;
  }
  else {
    printf("Sensor pressed\n");
    return 1;
  }
}

/*powers the Motor driver, docks the HDD, unpowers the motor driver.
* return values:
* 0: everything worked fine
* 1: timeout. Docking Sensor wasn't pushed within 3secs
* 2: already docked
*/
int dockHDD(float timeout) {
  if(sensor(SENSOR_DOCK)) {
    return 2;
  }
  relais(RELAIS_MOTOR, RELAIS_CLOSE);
  int return_value = 0;
  time_t time_before_dock = time(NULL);
  motor(FORWARD);
  while(sensor(SENSOR_DOCK) == 0) {
    if((time(NULL) - time_before_dock) > timeout) { /* replace 10 with more useful time contraint */
      return_value = 1;
      break;
    }
  }
  motor(BRAKE);
  relais(RELAIS_MOTOR, RELAIS_OPEN);
  return return_value;
}

/*powers the motor driver, undocks the HDD, unpowers the motor driver.
* return values:
* 0: everything worked fine
* 1: timeout. Undocking Sensor wasn't pushed within 3secs
* 2: already undocked
*/
int undockHDD(float timeout) {
  if(sensor(SENSOR_UNDOCK) == 1) {
    return 2;
  }
  relais(RELAIS_MOTOR, RELAIS_CLOSE);
  int return_value = 0;
  time_t time_before_undock = time(NULL);
  motor(BACKWARD);
  while(sensor(SENSOR_UNDOCK)==0) {
    if((time(NULL) - time_before_undock) > timeout) {
      return_value = 1;
      break;
    }
  }
  motor(BRAKE);
  relais(RELAIS_MOTOR, RELAIS_OPEN);
  return return_value;
}

/* sets the wheels in motion. No ADC support yet*/
void motor(int mode) {
  if(mode == FORWARD) {
    digitalWrite(MOTOR_TERMINAL1,1);
    digitalWrite(MOTOR_TERMINAL2,0);
  }

  else if(mode == BACKWARD) {
    digitalWrite(MOTOR_TERMINAL1,0);
    digitalWrite(MOTOR_TERMINAL2,1);
  }

  else {
    /* ground both terminals */
    digitalWrite(MOTOR_TERMINAL1,0);
    digitalWrite(MOTOR_TERMINAL2,0);
  }
  return;
}

/* closes relais 2 to power backup HDD if it is docked. Returns 1 if HDD is not docked */
int powerHDD() {
  if(sensor(SENSOR_DOCK) == 1) { 
    relais(RELAIS_HDD, RELAIS_CLOSE);
    return 0;
  }
  else {
    return 1;
  }
}

/* opens relais 2 to cut HDDs power supply */
void unpowerHDD() {
  relais(RELAIS_HDD, RELAIS_OPEN);
}