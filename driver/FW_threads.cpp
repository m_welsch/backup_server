/*
* compile with g++ FW_threads.cpp -o FW_threads -lpthread -lwiringPi
* sources: http://www.tutorialspoint.com/cplusplus/cpp_multithreading.htm
*          http://www.binarytides.com/server-client-example-c-sockets-linux/
*		   https://stackoverflow.com/questions/6175502/how-to-parse-ini-file-with-boost
*
* Todo:
* mutex/semaphore for the flags (https://computing.llnl.gov/tutorials/pthreads/)
*/

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <wiringPi.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

/* BPi pin definitions */
#define RELAIS_MOTOR 0
#define RELAIS_HDD 1
#define MOTOR_TERMINAL1 2
#define MOTOR_TERMINAL2 3
#define SENSOR_DOCK 4
#define SENSOR_UNDOCK 5
#define BUTTON 4

#define RELAIS_OPEN 0
#define RELAIS_CLOSE 1

#define BACKWARD 2
#define FORWARD 1
#define BRAKE 0

/* complex datatypes */
struct SIGNAL_FLAGS {
	int server_port;
	int flag_running;
	int flag_button_pressed;
	int flag_kill;
	int flag_dockHDD;
	int flag_undockHDD;
	int flag_powerHDD;
	int flag_unpowerHDD;
};

struct CONNECTION_INFO {
	int socket;
	void *pointer_to_flags;
};

/* function prototypes */
void dostuff (int sock, void *address_of_flags);
void *connection_handler(void *);
void init_ports(void);
void relais(int relais, int mode);
int sensor (int sensor_pin);
int dockHDD(float timeout);
int undockHDD(float timeout);
void motor(int mode);
int powerHDD(void);
void unpowerHDD(void);

void error(const char *msg)
{
    // perror(msg);
    std::cout << msg;
    exit(1);
}

void *wait_for_signal(void *address_of_flags) {

	struct SIGNAL_FLAGS *myflags = (struct SIGNAL_FLAGS *) address_of_flags;

	int socket_desc , client_sock , c;
    struct sockaddr_in server , client;
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        std::cout << "Could not create socket" << std::endl;
    }

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( myflags->server_port );
     
    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        error("bind failed. Error");
        exit(-1);
    }

    //Listen
    listen(socket_desc , 3);
     
    //Accept and incoming connection
    c = sizeof(struct sockaddr_in);

	pthread_t thread_id;
	
    while( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        struct CONNECTION_INFO connection_info;
        connection_info.pointer_to_flags = address_of_flags;
        connection_info.socket = client_sock;
         
        if( pthread_create( &thread_id , NULL ,  connection_handler , (void*) &connection_info) < 0)
        {
            error("could not create thread");
			exit(-1);
        }
         
        //Now join the thread , so that we dont terminate before the thread
        if(myflags->flag_kill) {
        	pthread_join( thread_id , NULL);
        }
    }
     
    if (client_sock < 0)
    {
        error("accept failed");
        exit(-1);
    }
}

void *connection_handler(void *connection_info_address)
{
    struct CONNECTION_INFO *connection_info = (struct CONNECTION_INFO*) connection_info_address;
    struct SIGNAL_FLAGS *myflags = (struct SIGNAL_FLAGS *) connection_info->pointer_to_flags; 

    //Get the socket descriptor
    int sock = connection_info->socket;
    int read_size;
    char *message , client_message[2000];
    int message_code = 0;
     
    //Send some messages to the client
    message = (char *) "Greetings! I am your connection handler\n";
    write(sock , message , strlen(message));
     
    //Receive a message from client
    while( (read_size = recv(sock , client_message , 2000 , 0)) > 0 )
    {
        //end of string marker
		client_message[read_size] = '\0';
		
    	message_code = atoi(client_message); //TODO: more secure way of determining the message code ...
    	std::cout << "Messagecode: " << message_code << std::endl;

    	switch(message_code) {
    		case 1:
   				std::cout << "Received code 1, setting flag for dockHDD()\n";
   				write(sock,"Received code 1, setting flag for dockHDD()\n",45);
   				myflags->flag_dockHDD = 1;
   				break;

	   		case 2:
   				std::cout << "Received code 2, setting flag for undockHDD()\n";
   				write(sock,"Received code 2, setting flag for undockHDD()\n",47);
		      	myflags->flag_undockHDD = 1;
		   		break;

		    case 3:
   				std::cout << "Received code 3, setting flag for powerHDD()\n";
   				write(sock,"Received code 3, setting flag for powerHDD()\n",45);
		      	myflags->flag_powerHDD = 1;
		   		break;

		    case 4:
   				std::cout << "Received code 4, setting flag for unpowerHDD()\n";
   				write(sock,"Received code 4, setting flag for unpowerHDD()\n",47);
		      	myflags->flag_unpowerHDD = 1;
		   		break;

		   	case 5:
		      	/* turn everything on */
		      	digitalWrite(RELAIS_MOTOR,1);
		      	digitalWrite(RELAIS_HDD,1);
		      	digitalWrite(MOTOR_TERMINAL1,1);
		      	digitalWrite(MOTOR_TERMINAL2,1);
		      	delay(1000);
		      	/* turn everything off */
		      	digitalWrite(RELAIS_MOTOR,0);
		      	digitalWrite(RELAIS_HDD,0);
		      	digitalWrite(MOTOR_TERMINAL1,0);
		      	digitalWrite(MOTOR_TERMINAL2,0);
		   		break;

    		case 7:
    			std::cout << "Received message code 7, setting kill flag.\n";
    			myflags->flag_kill = 1;
    			break;

    		default:
    			std::cout << "Unknown message code: " << message_code << std::endl;
    			write(sock, "Unknown message code\n",21);
    	}

		//Send the message back to client
        write(sock , client_message , strlen(client_message));
        write(sock, "Test Signal Dings\n",18);
        std::cout << "client_message: " << client_message << std::endl;
		
		//clear the message buffer
		memset(client_message, 0, 2000);
    }
     
    // if(read_size == 0)
    // {
    //     fflush(stdout);
    // }
    // else if(read_size == -1)
    // {
    //     error("recv failed");
    // }
         
}

int main() {
	int rc;

	/* init control flags */
	struct SIGNAL_FLAGS flags;
	flags.flag_running = 1;
	flags.flag_kill = 0;
	flags.flag_button_pressed = 0;
	flags.flag_dockHDD = 0;
	flags.flag_undockHDD = 0;
	flags.flag_powerHDD = 0;
	flags.flag_unpowerHDD = 0;

	/* init HW interface */
	init_ports();

	/* read config file */
	boost::property_tree::ptree config_file;
	boost::property_tree::ini_parser::read_ini("../config", config_file);
	//std::cout << "Firmware Server Port: " << pt.get<std::string>("Firmware.Server_Port") << std::endl;
	flags.server_port = config_file.get<int>("Firmware.Server_Port");

	/* init threads */
	pthread_t threads[1];
   	pthread_attr_t attr;
	void *status;

	/* Initialize and set thread joinable */
   	pthread_attr_init(&attr);
   	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

   	/* create server */
   	rc = pthread_create(&threads[1], &attr, wait_for_signal, (void *) &flags);
   	if (rc) {
    	std::cout << "Error:unable to create thread," << rc << std::endl;
        exit(-1);
    }

    /* mainloop */
    int iterator = 0;
    while(flags.flag_running) {
    	if(digitalRead(BUTTON) == 0) {
      		std::cout << "button press detected, setting flag." << std::endl;
			flags.flag_button_pressed = 1;
		}
    	if(digitalRead(SENSOR_UNDOCK) == 0) {
      		flags.flag_kill = 1;
    	}

    	if(flags.flag_kill) {
    		std::cout << "shutting down\n";
    		flags.flag_running = 0;
    	}
    	if(flags.flag_button_pressed) {
    		flags.flag_button_pressed = 0;
    		std::cout << "button pressed\n";
    	}
    	if(flags.flag_dockHDD) {
    		flags.flag_dockHDD = 0;
    		std::cout << "read flag_dockHDD: perfoming dockHDD()\n";
      		dockHDD(100);
    	}
    	if(flags.flag_undockHDD) {
    		flags.flag_undockHDD = 0;
    		std::cout << "read flag_undockHDD: perfoming undockHDD()\n";
      		undockHDD(100);
    	}
    	if(flags.flag_powerHDD) {
    		flags.flag_powerHDD = 0;
    		std::cout << "read flag_powerHDD: perfoming powerHDD()\n";
      		powerHDD();
    	}
    	if(flags.flag_unpowerHDD) {
    		flags.flag_unpowerHDD = 0;
    		std::cout << "read flag_unpowerHDD: perfoming unpowerHDD()\n";
      		unpowerHDD();
    	}
    	if(iterator==10) {
    		std::cout << "flags.flag_kill = " << flags.flag_kill << ", &flags.flag_kill = " << &flags.flag_kill << std::endl;
    		iterator = 0;
    	}
    	//else iterator++;
    	delay(100);
    }

    /* free attribute and wait for the other threads */
    std::cout << "initiating shutdown\n";
	pthread_attr_destroy(&attr);

    rc = pthread_join(threads[0], &status);
		
    if (rc){
        std::cout << "Error:unable to join," << rc << std::endl;
        exit(-1);
    }

    pthread_exit(NULL);
}

void init_ports(void) {
  wiringPiSetup();

  /* Relais open at default */
  pinMode(RELAIS_MOTOR,OUTPUT);
  pinMode(RELAIS_HDD,OUTPUT);
  digitalWrite(RELAIS_MOTOR, RELAIS_OPEN);
  digitalWrite(RELAIS_HDD, RELAIS_OPEN);

  /* Motor Driver */
  pinMode(MOTOR_TERMINAL1,OUTPUT);
  pinMode(MOTOR_TERMINAL2,OUTPUT);
  digitalWrite(MOTOR_TERMINAL1, 0);
  digitalWrite(MOTOR_TERMINAL2, 0);

  //pinMode(14,OUTPUT);

  /* turn the internal pullup-resistors for the sensor-pins on */
  pinMode(SENSOR_DOCK,INPUT);
  pinMode(SENSOR_UNDOCK,INPUT);
  pullUpDnControl(SENSOR_DOCK, PUD_UP);
  pullUpDnControl(SENSOR_UNDOCK, PUD_UP);

  /* next line sets pin1 in pwm mode */
  // pinMode(1,2);

  /* set GPIO PIN 2 (16 on Board) as Input for the ADC sense */
  // pinMode(16,0);
}

/* Closes the relais with mode 1 or opens it with mode 0 */
void relais(int relais, int mode) {
  /* the Relaisboard contains an inverter, so a logical LOW results in an open relais vice versa */
  if(mode == 0) {
    digitalWrite(relais,0);
  }
  else if(mode == 1) {
    digitalWrite(relais,1);
  }
  return;
}

/* Returns 1 if sensor on sensor_pin is pressed, otherwise 0 */
int sensor (int sensor_pin) {
  if(digitalRead(sensor_pin)==1) {
    return 0;
  }
  else {
    std::cout << "Sensor pressed\n";
    return 1;
  }
}

/*powers the Motor driver, docks the HDD, unpowers the motor driver.
* return values:
* 0: everything worked fine
* 1: timeout. Docking Sensor wasn't pushed within timeout
* 2: already docked
*/
int dockHDD(float timeout) {
  if(sensor(SENSOR_DOCK)) {
    return 2;
  }
  relais(RELAIS_MOTOR, RELAIS_CLOSE);
  int return_value = 0;
  time_t time_before_dock = time(NULL);
  motor(FORWARD);
  while(sensor(SENSOR_DOCK) == 0) {
    if((time(NULL) - time_before_dock) > timeout) {
      return_value = 1;
      break;
    }
  }
  motor(BRAKE);
  relais(RELAIS_MOTOR, RELAIS_OPEN);
  return return_value;
}

/*powers the motor driver, undocks the HDD, unpowers the motor driver.
* return values:
* 0: everything worked fine
* 1: timeout. Undocking Sensor wasn't pushed within timeout
* 2: already undocked
*/
int undockHDD(float timeout) {
  if(sensor(SENSOR_UNDOCK) == 1) {
    return 2;
  }
  relais(RELAIS_MOTOR, RELAIS_CLOSE);
  int return_value = 0;
  time_t time_before_undock = time(NULL);
  motor(BACKWARD);
  while(sensor(SENSOR_UNDOCK)==0) {
    if((time(NULL) - time_before_undock) > timeout) {
      return_value = 1;
      break;
    }
  }
  motor(BRAKE);
  relais(RELAIS_MOTOR, RELAIS_OPEN);
  return return_value;
}

/* sets the wheels in motion. No ADC support yet*/
void motor(int mode) {
  if(mode == FORWARD) {
    digitalWrite(MOTOR_TERMINAL1,1);
    digitalWrite(MOTOR_TERMINAL2,0);
  }

  else if(mode == BACKWARD) {
    digitalWrite(MOTOR_TERMINAL1,0);
    digitalWrite(MOTOR_TERMINAL2,1);
  }

  else {
    /* ground both terminals */
    digitalWrite(MOTOR_TERMINAL1,0);
    digitalWrite(MOTOR_TERMINAL2,0);
  }
  return;
}

/* closes relais 2 to power backup HDD if it is docked. Returns 1 if HDD is not docked */
int powerHDD() {
  if(sensor(SENSOR_DOCK) == 1) { 
    relais(RELAIS_HDD, RELAIS_CLOSE);
    return 0;
  }
  else {
    return 1;
  }
}

/* opens relais 2 to cut HDDs power supply */
void unpowerHDD() {
  relais(RELAIS_HDD, RELAIS_OPEN);
}