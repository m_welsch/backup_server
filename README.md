## README ##

# Backup-Server Software #


### What is this repository for? ###

This repository contains the server-side software for a project with working title *Backup-Server*.

Concerning hardware, the *Backup-Server* consists of a Raspberry Pi, a 3,5" HDD and an electric motor driven mechanism to unplug the HDD from the rest to ensure galvanic isolation of the HDD in order to minimize lightning strike damage to it.

This repository provides the software for the *Backup-Server*. It is intended to run on the Raspberry Pi with installed Raspbian and to:

* schedule and execute backups
* considering local weather forecasts and
* manage the decoupling mechanism.