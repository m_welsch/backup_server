@ECHO off

set WORKING_DIRECTORY=%cd%
"C:\Program Files\Python\Scripts\pyside-uic.exe" "%WORKING_DIRECTORY%\MainWindow.ui" -o "%WORKING_DIRECTORY%\MainWindow.py"
"C:\Program Files\Python\Scripts\pyside-uic.exe" "%WORKING_DIRECTORY%\MainWindow.ui" -o "%WORKING_DIRECTORY%\MainWindowTmp.py" -x

echo #!/usr/bin/env python3 > "%WORKING_DIRECTORY%\MainWindow-x.py"
type "%WORKING_DIRECTORY%\MainWindowTmp.py" >> "%WORKING_DIRECTORY%\MainWindow-x.py"

del "%WORKING_DIRECTORY%\MainWindowTmp.py"

REM Pause