#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, time
import threading
from PySide import QtCore, QtGui
import MainWindow
import configparser
import paramiko

class Window(QtGui.QMainWindow):
	def __init__(self, parent=None):
		QtGui.QMainWindow.__init__(self, parent)
		self.ui = MainWindow.Ui_MainWindow()
		self.ui.setupUi(self)
		self.netThread = NetworkThread()
		self.netThread.connectionEstablished.connect(self.finishConnectionEstablishment)
		self.netThread.sshExecuted.connect(self.finishSSHExecution)
		self.statusText = QtGui.QLabel("No connection established.")
		self.statusBar().addWidget(self.statusText, 1)
		self.setConnectButtonVisibility(True, False, False)

		self.settings = self.loadSettings()
		self.serverAddress = self.settings["Global"]["last_server"]
		self.username = self.settings["Global"]["username"]
		self.password = self.settings["Global"]["password"]
		self.autoConnect = True if self.settings["Global"]["auto_connect"]=="True" else False
		self.savePW = True if self.settings["Global"]["save_password"]=="True" else False

		self.ui.eAddress.setText(self.serverAddress)
		self.ui.eUsername.setText(self.username)
		self.ui.ePassword.setText(self.password)
		self.ui.cbAutoConnect.setChecked(self.autoConnect)
		self.ui.cbSavePW.setChecked(self.savePW)

		self.ui.bAddressConnect.clicked.connect(self.bAddressConnectCB)
		self.ui.bAddressDisconnect.clicked.connect(self.bAddressDisconnectCB)
		self.ui.bAddressCancel.clicked.connect(self.bCancelConnectionProcessCB)
		self.ui.bDebugDock.clicked.connect(self.debugDockTest)
		self.ui.bBackupNow.clicked.connect(self.performBackupNow)

		self.ui.eAddress.returnPressed.connect(self.bAddressConnectCB)
		self.ui.eUsername.returnPressed.connect(self.bAddressConnectCB)
		self.ui.ePassword.returnPressed.connect(self.bAddressConnectCB)

		if self.autoConnect:
			self.connectToAddress()


	def setStatus(self, text):
		self.statusText.setText(text)

	def loadSettings(self):
		cp = configparser.ConfigParser()
		if not os.path.isfile("settings.ini"):
			cp["Global"] = {"last_server": "",
							"username": "",
							"password": "",
							"auto_connect": "True",
							"save_password": "False"}
			try:
				with open("settings.ini", "w") as file:
					cp.write(file)
			except:
				print("Could not create file 'settings.ini'. Exiting...")
				sys.exit()
		else:
			try:
				cp.read('settings.ini')
			except:
				print("Could not read file 'settings.ini'. Exiting...")
				sys.exit()
		return cp

	def saveSettings(self):
		try:
			with open("settings.ini", "w") as file:
				self.settings.write(file)
		except:
			print("Could not save settings to 'settings.ini'. Exiting...")
			sys.exit()

	def bAddressConnectCB(self):
		self.serverAddress = self.ui.eAddress.text()
		self.username = self.ui.eUsername.text()
		self.password = self.ui.ePassword.text()
		self.connectToAddress()

	def bAddressDisconnectCB(self):
		self.stopNetThread()
		self.setStatus("Connection to "+self.serverAddress+" cut.")

	def stopNetThread(self):
		self.netThread.kill()
		self.setConnectionControlsAbility(True)
		self.setConnectButtonVisibility(True, False, False)

	def bCancelConnectionProcessCB(self):
		print("Not yet implemented.")
		# self.netThread.sshClient.close()  # Doesn't work that way.
		# self.netThread.terminate()  # Works technically, but messes up the internal state and the GUI.

	def connectToAddress(self):
		self.setConnectionControlsAbility(False)
		self.setConnectButtonVisibility(False, False, True)
		self.settings["Global"]["username"] = self.username
		self.settings["Global"]["password"] = self.password if self.ui.cbSavePW.isChecked() else ""
		self.settings["Global"]["auto_connect"] = "True" if self.ui.cbAutoConnect.isChecked() else "False"
		self.settings["Global"]["save_password"] = "True" if self.ui.cbSavePW.isChecked() else "False"
		self.saveSettings()
		self.setStatus("Connecting to "+self.serverAddress+"...")
		self.netThread.start()
		self.netThread.establishConnection(self.serverAddress, self.username, self.password)
		self.netThread.lsTest()

	def finishConnectionEstablishment(self, errorcode):
		if not errorcode:
			self.settings["Global"]["last_server"] = self.serverAddress
			self.saveSettings()
			self.setStatus("Connected with "+self.serverAddress+". No Backup done yet.")
			self.setConnectButtonVisibility(False, True, False)
		else:
			if errorcode == 1: self.setStatus("Error: Authentication failed.")
			if errorcode == 2: self.setStatus("Error: "+self.serverAddress+" is no valid connection.")
			self.stopNetThread()

	def setConnectionControlsAbility(self, state):
		self.ui.eAddress.setEnabled(state)
		self.ui.eUsername.setEnabled(state)
		self.ui.ePassword.setEnabled(state)
		self.ui.cbAutoConnect.setEnabled(state)
		self.ui.cbSavePW.setEnabled(state)

	def setConnectButtonVisibility(self, a, b, c):
		self.ui.bAddressConnect.setVisible(a)
		self.ui.bAddressDisconnect.setVisible(b)
		self.ui.bAddressCancel.setVisible(c)

	def sshExec(self, command, out=False):
		self.netThread.executeSSH(command, out)

	def finishSSHExecution(self, successful):
		pass

	def debugDockTest(self):
		self.sshExec("sudo python /home/pi/backup-program/backup-program.py -t docking")

	def performBackupNow(self):
		self.sshExec("sudo python /home/pi/backup-program/backup-program.py -t backup")



#  Obsolete, maybe needed later for other use
# class EventHandler(QtCore.QObject):
	# def __init__(self, wid):
		# QtCore.QObject.__init__(self)
		# self.wid = wid

	# def eventFilter(self, obj, e):
		# print("event detected")
		# if (obj == self.wid.ui.eAddress or obj == self.wid.ui.eUsername or obj == self.wid.ui.ePassword) and e.type() == QtCore.QEvent.ShortcutOverride and e.key() == QtCore.Qt.Key_Return:
			# self.wid.connectToAddress()
			# return QtCore.QObject.eventFilter(self, obj, e)
		# else:
			# return QtCore.QObject.eventFilter(self, obj, e)



class NetworkThread(QtCore.QThread):

	connectionEstablished = QtCore.Signal(int)
	sshExecuted = QtCore.Signal(bool)

	def __init__(self):
		QtCore.QThread.__init__(self)
		self.connection = False
		self.exitFlag = self.connEstFlag = self.sshExecFlag = self.debugFlag = False
		self.sshClient = self.command = self.out = None
		self.serverAddress = self.username = self.password = None

	def run(self):
		while True:
			if self.exitFlag:
				# print("Exiting...")
				self.exitFlag = False
				self.sshClient.close()
				self.connection = False
				# print("About to exit.")
				break
			if self.connEstFlag:
				# print("Establishing...")
				self.connEstFlag = False
				self.sshClient = paramiko.client.SSHClient()
				self.sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
				self.sshClient.load_system_host_keys()
				try:
					self.sshClient.connect(self.serverAddress, username=self.username, password=self.password)
					self.connection = True
				except Exception as e:
					if type(e) == paramiko.ssh_exception.AuthenticationException:
						self.connectionEstablished.emit(1)
					else: self.connectionEstablished.emit(2)
					# print("Establishing failed.")
					continue
				self.connectionEstablished.emit(0)
				# print("Established.")
			if self.sshExecFlag:
				# print("Executing...")
				self.sshExecFlag = False
				if self.connection:
					stdin, stdout, stderr = self.sshClient.exec_command(self.command)
					stdout.channel.recv_exit_status()
					if self.out:
						lines = stdout.readlines()
						for line in lines:
							print(line, end="")
						print()
					self.sshExecuted.emit(True)
				# print("Executed.")
			if self.debugFlag:
				self.debugFlag = False
				self.executeSSH("ls -l", True)
			else:
				time.sleep(0.1)
				# print("idle")

	def kill(self):
		self.exitFlag = True

	def establishConnection(self, serverAddress, username, password):
		self.serverAddress = serverAddress
		self.username = username
		self.password = password
		self.connEstFlag = True

	def executeSSH(self, command, out):
		self.command = command
		self.out = out
		self.sshExecFlag = True

	def lsTest(self):
		self.debugFlag = True



def main():
	##### Init App ################################################################
	app = QtGui.QApplication(sys.argv)

	##### Setup GUI ###############################################################
	wid = Window()

	##### Signals & Slots #########################################################
	#app.aboutToQuit.connect(lambda: saveData(ui, wid.yearData, ui.calendar.selectedDate()))

	##### Start ###################################################################
	wid.show()

	sys.exit(app.exec_())


main()